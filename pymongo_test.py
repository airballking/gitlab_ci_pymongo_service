#!/usr/bin/env python
from pymongo import MongoClient


if __name__ == '__main__':
  # code taken from here: https://realpython.com/introduction-to-mongodb-and-python/
  mongo = MongoClient(host='mongo', port=27017)
  db = mongo.pymongo_test
  posts = db.posts
  post_data = {
    'title': 'Python and MongoDB',
    'content': 'PyMongo is fun, you guys',
    'author': 'Scott'
  }
  result = posts.insert_one(post_data)
  print('One post: {0}'.format(result.inserted_id))
  scots_post = posts.find_one({'author': 'Scott'})
  print(scots_post)

